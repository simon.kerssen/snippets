from dialog import Dialog
import os
import logging

# Logger
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

# Dialog
# You may want to use 'autowidgetsize=True' here (requires pythondialog >= 3.1)
d = Dialog(dialog="dialog")
# Dialog.set_background_title() requires pythondialog 2.13 or later
d.set_background_title("Soil2Liquid Simulator")

# main loop
while True:
    # Dialog-Example: http://pythondialog.sourceforge.net/
    code, tag = d.menu("This are your options:",
                       choices=[("(1)", "Set GPIOs to default state"),
                                ("(2)", "Run INIT")])
    if code == d.OK:
        # clear screen so we can see the logger output
        os.system('clear')

        # 'tag' is now either "(1)" or "(2)"
        if tag == "(1)":
            # set to default state
            logger.debug("Set GPIOs to default state ✅")
            input("Press Enter to continue...")
        elif tag == "(2)":
            logger.debug("Run INIT ❌")
            input("Press Enter to continue...")
    else:  # cancle was selected
        break

# clear screen
os.system('clear')
exit()
